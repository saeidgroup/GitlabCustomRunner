# DevEnvSetup

This repository is mainly for setting up a development environment with libvirt/qemu/KVM 
virtual machines, ansible utilities, including molecule for testing ansible roles, with 
libvirt provider, and setting up a custom gitlab runner service with libvirt backend that 
can accept CI/CD jobs.

## Quickstart guide

For a quick setup in a machine with `dnf` package manager, run the `setup.sh` script:
- `git clone git@gitlab.com:saeidgroup/DevEnvSetup.git`
- `cd DevEnvSetup && ./setup.sh`

This will: 
- install and setup libvirt/qemu/KVM and vagrant virtualization stack.
- install ansible utilities, including molecule and it's libvirt/vagrant backend.
- setup a shell, dockere and a custom gitlab runner with libvirt driver, 
for CI/CD pipelines inside a KVM VM with libvirt API.

**Note**: for registering the gitlab custom runner, the runner token have to be obtained 
from gitlab web interface, and have to be inserted on prompt.

```
Usage: ./setup.sh [options]
Install libvirt/qemu/KVM, ansible/molecule stack, build a base image, and
register a custom gitlab CI/CD runner with libvirt virtual machines backend.

options:
  --virts     install libvirt/qemu/KVM stack.
  --ansible   install ansible and molecule stack.
  --build     build a base qemu image for gitlab's CI/CD pipeline.
  --register  register a shell, docker and a custom gitlab runner with libvirt driver.
  --help|-h   print this help message.

defaults to all of above in order.
```

### Molecule setup for local testing

Here we assume that the user can spawn KVM VMs using vagrant with libvirt provider. 
To be able to test ansible roles using ansible molecule with a lbvirt driver, install 
the required python packages, preferably inside a python virtual environment:
- `pip install -r requirements.txt`

then issue `molecule test -s default` inside the ansible role directory containing the molecule directory.

Useful molecule commands:
```bash
# create the instances (for us the VMs using vagrant-libvirt)
molecule create
# list the created instances
molecule list
# login to the instance
molecule login
# run the converge playbook
molecule converge
# check the idempotence of the role
molecule idempotence
# run the whole test sequence
molecule test
# destroy the instance
molecule destroy
# reset the configurations (in case of changes)
molecule reset
```

An example molecule scenario is given in [molecule-templates](./molecule-templates) directory, 
along with a barebone default `testinfra` python unit test.

### Registering a custom gitlab runner with a libvirt driver

Gitlab's shared runners don't support spawning VMs using libvirt, for that we have 
to register a custom runner. Depending on the distribution, `gitlab-runner` package 
should be installed, for Fedora:
- `curl -fsL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash -`
- `sudo dnf install -y gitlab-runner`

A registration token have to obtained through Gitlab web interface for a custom runner, 
either for the whole gitlab instance (if it's a self-hosted gitlab instance), for a 
group of projects, or per project (under settings, in CI/CD section, runners), 
a `gitlab-runner` user must also exists, then initiate the registration of a 
machine that will be used to spawn the VMs:
```bash
sudo gitlab-runner register --non-interactive \
    --url "https://gitlab.com" \
    --token "${LIBIVRT_RUNNER_TOKEN}" \
    --executor "custom" \
    --builds-dir "/home/gitlab-runner/builds" \
    --cache-dir "/home/gitlab-runner/cache" \
    --description "Libvirt Runner" \
    --custom-prepare-exec "/opt/gitlab-runners/libvirt-driver/prepare.sh" \
    --custom-run-exec "/opt/gitlab-runners/libvirt-driver/run.sh" \
    --custom-cleanup-exec "/opt/gitlab-runners/libvirt-driver/cleanup.sh"
```
Check the `gitlab-runner.service`, it should have started with no complaints about the builtin http server.
Sample `prepare.sh`, `run.sh`, and `cleanup.sh` scripts are provided inside [helper-scripts](./helper-scripts), 
these will be used by the runner service to manage the VMs, note that `base.sh` script also should be put 
in the same directory as others, as it will be sourced by these scripts. Also for speeding up spawning the VMs, 
a base image should be created which the runner VM will be based off of. A sample `base_image_builder.sh` 
script that creates a debian image using `virt-builder` with some starter packages is in the same directory.
Note that with gitlab-runner installed, the jobs can be run also locally with `gitlab-runner exec shell test-job`.

**Notes**: 
- the script will register shell, docker and libvirt runners with gitlab.com instance,
when getting a specific registration token from gitlab, also add a unique tag to that runner,
to choose which runner should accept a job in .gitlab-ci.yml, add the runner tag to that job.
- the script will add the gitlab-runner user to the wheel group also, 
which in principal members of it should be able to run sudo commands 
without password (should be set in sudoers file).
- for now the base image used for the libvirt runner is debian, this can be changed in 
the script, but then again all the initial packages installed should be also modified accordingly.
