#!/usr/bin/env bash
#
# prepare the virtual machine that the will run the jobs 
# spawned by gitlab CI/CD pipeline and a custom driver,
# gitlab runner's config.toml file prepare argument 
# should point to this file.

PRIVATE_SSH_KEY="/home/saliei/.ssh/id_rsa"

CURDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source "${CURDIR}/base.sh"

set -eo pipefail
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

qemu-img create -f qcow2 -b "$BASE_VM_IMAGE" "$VM_IMAGE" -F qcow2
virt-install \
    --name "$VM_ID" \
    --os-variant debian12 \
    --disk "$VM_IMAGE" \
    --import \
    --vcpus=2 \
    --ram=4096 \
    --network default \
    --graphics none \
    --noautoconsole \
    --cpu host \
    --memorybacking access.mode=shared \
    --filesystem type=mount,mode=passthrough,driver.type=virtiofs,source=/opt/tf-state,target=tf-state-dir

echo 'Waiting for VM to get IP'
for i in $(seq 1 120); do
    VM_IP=$(_get_vm_ip)

    if [ -n "$VM_IP" ]; then
        echo "VM got IP: $VM_IP"
        break
    fi
    if [ "$i" == "120" ]; then
        echo 'Waited 120 seconds for VM to start, exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
    sleep 1s
done

echo "Waiting for sshd to be available"
for i in $(seq 1 120); do
    if ssh -q -i "$PRIVATE_SSH_KEY" -o StrictHostKeyChecking=no gitlab-runner@"$VM_IP" exit; then
        break
    fi
    if [ "$i" == "120" ]; then
        echo 'Waited 120 seconds for sshd to start, exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
    sleep 1s
done

echo "Mounting tf-state-dir at /opt/tf-state"
ssh -i "$PRIVATE_SSH_KEY" -o StrictHostKeyChecking=no gitlab-runner@"$VM_IP" "/usr/bin/sudo mount -t virtiofs tf-state-dir /opt/tf-state"
