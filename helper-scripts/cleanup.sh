#!/usr/bin/env bash
#
# helper script to be used by gitlab runner to destroy 
# the VM after it has finished running the job, 
# gitlab runner's config.toml file cleanup argument 
# should point to this file.

CURDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source "${CURDIR}/base.sh"

set -eo pipefail

virsh shutdown "$VM_ID"
virsh undefine "$VM_ID"

if [ -f "$VM_IMAGE" ]; then
    rm -f "$VM_IMAGE"
fi
