#!/usr/bin/env bash
#
# standalone script for building qemu images using virt-install utility.

BLACK="\033[0;30m"
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
PURPLE="\033[0;35m"
CYAN="\033[0;36m"
WHITE="\033[0;37m"
BGREEN="\033[1;32m"
BRED="\033[1;31m"
RESET="\033[0m"

CURRENT_DIR="$(basename $0)"
LIBVIRT_SCRIPTS_PATH="/opt/libvirt-driver"

DEFAULT_SSH_PUBLIC_KEY="$HOME/.ssh/id_rsa.pub"
DEFAULT_SSH_PRIVATE_KEY="$HOME/.ssh/id_rsa"
DEFAULT_IMAGE_ROOT_PASSWORD="1234"
DEFAULT_BASE_IMAGE="debian-12"
DEFAULT_BASE_IMAGE_PATH="/var/lib/libvirt/images/gitlab-runner-base-${DEFAULT_BASE_IMAGE}.qcow2"
DEFAULT_BASE_SIZE="12G"
DEFAULT_BASE_HOSTNAME="gitlab-runner-${DEFAULT_BASE_IMAGE}"

trap "DIE" SIGINT
trap "DIE" SIGQUIT

function DIE() {
    echo -e "\nsetup.sh: exiting on interrupt..."
    echo "removing any temporary files"
    sudo rm -f "${LIBVIRT_SCRIPTS_PATH}/tmp-*.tmp"
    sudo rm -f "$(dirname $DEFAULT_BASE_IMAGE_PATH)/tmp-*.tmp"
    exit 127
}

function LOG() {
    CURDATE="${BLUE}$(date +'%Y-%m-%d %T')${RESET}"
    LOGGER="setup"
    ORIG_LOGLEVEL="$1"

    case $1 in
        "DEBUG")
            shift
            LOGLEVEL="${GREEN}DEBUG${RESET}"
            ;;
        "INFO")
            shift
            LOGLEVEL="${CYAN} INFO${RESET}"
            ;;
        "WARN")
            shift
            LOGLEVEL="${YELLOW} WARN${RESET}"
            ;;
        "ERROR")
            shift
            LOGLEVEL="${RED}ERROR${RESET}"
            ;;
        "FATAL")
            shift
            LOGLEVEL="${BRED}FATAL${RESET}"
            ;;
        *)
            LOGLEVEL="${WHITE}NOLEVEL${RESET}"
            ;;
    esac

    LOGMSG="$1"
    echo -e "$CURDATE $LOGGER $LOGLEVEL: $LOGMSG"
    #[[ "$ORIG_LOGLEVEL" == "FATAL" ]] && DIE
}

function _create_default_ssh_key() {
    if [[ ! -f "${DEFAULT_SSH_PUBLIC_KEY}" ]] || [[ ! -f "${DEFAULT_SSH_PRIVATE_KEY}" ]]; then
        LOG INFO "default ssh key not present, creating: $HOME/.ssh/id_rsa"
        ssh-keygen -t rsa -b 2048 -f "${DEFAULT_SSH_PRIVATE_KEY}" -N ""
    fi
}

function _build_image() {
    _debug_str="$1"
    _root_password="$2"
    _ssh_public_key="$3"
    _base_image="$4"
    _base_size="$5"
    _base_path="$6"
    _base_hostname="$7"
    LOG DEBUG "starting virt-builder"
    sudo virt-builder "${_debug_str}" "${_base_image}" \
        --size "${_base_size}" \
        --output "${_base_path}" \
        --hostname "${_base_hostname}" \
        --format qcow2 \
        --network \
        --install curl,sudo,xsltproc,gnupg,software-properties-common,gnupg,xsltproc,genisoimage \
        --install ca-certificates,gcc,iproute2,bzip2,gzip,zip,unzip,tar,xz-utils,openssh-server,libssl-dev \
        --install procps,pkg-config,flake8,yamllint,python3,python3-pip,python3-virtualenv \
        --install qemu-system,libvirt-clients,libvirt-daemon-system,libvirt-dev \
        --run-command 'curl -fsSL https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg' \
        --run-command 'curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg' \
        --run-command 'gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint' \
        --run-command 'gpg --no-default-keyring --keyring /usr/share/keyrings/docker-archive-keyring.gpg --fingerprint' \
        --run-command 'echo \
                            "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com \
                            "$(. /etc/os-release && echo "$VERSION_CODENAME")" main" | \
                            tee /etc/apt/sources.list.d/hashicorp.list' \
        --run-command 'echo \
                            "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
                            "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
                            tee /etc/apt/sources.list.d/docker.list' \
        --run-command 'curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash' \
        --run-command 'curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh" | bash' \
        --run-command 'useradd -m -p "" gitlab-runner -s /bin/bash' \
        --run-command 'apt update' \
        --install git,git-lfs \
        --install vagrant,terraform \
        --install docker,docker-ce,docker-ce-cli,containerd.io,docker-buildx-plugin,docker-compose-plugin \
        --run-command "git lfs install --skip-repo" \
        --run-command "mkdir -p /opt/tf-state" \
        --run-command "chown -R gitlab-runner:gitlab-runner /opt/tf-state" \
        --run-command "chmod -R 775 /opt/tf-state" \
        --ssh-inject gitlab-runner:file:"${_ssh_public_key}" \
        --run-command "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" \
        --run-command "sed -E 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/' -i /etc/default/grub" \
        --run-command "grub-mkconfig -o /boot/grub/grub.cfg" \
        --run-command "echo 'auto eth0' >> /etc/network/interfaces" \
        --run-command "echo 'allow-hotplug eth0' >> /etc/network/interfaces" \
        --run-command "echo 'iface eth0 inet dhcp' >> /etc/network/interfaces" \
        --run-command "echo 'export PATH=/home/gitlab-runner/.local/bin:/usr/local/bin:/usr/bin:/bin' >> /home/gitlab-runner/.bashrc" \
        --run-command "echo 'PY_COLORS=1' >> /home/gitlab-runner/.bashrc" \
        --run-command "echo 'ANSIBLE_FORCE_COLOR=1' >> /home/gitlab-runner/.bashrc" \
        --run-command "adduser gitlab-runner libvirt" \
        --run-command "systemctl enable libvirtd docker" \
        --root-password password:"${_root_password}"
}

function build_base_image() {
    LOG DEBUG "starting base image builder"
    LOG INFO "reading ssh public key path: SSH_PUBLIC_KEY"
    if [[ -z "${SSH_PUBLIC_KEY}" ]]; then
        LOG WARN "SSH_PUBLIC_KEY env not set, using default one: ${DEFAULT_SSH_PUBLIC_KEY}"
        _create_default_ssh_key
        _ssh_public_key=${DEFAULT_SSH_PUBLIC_KEY}
        [[ ! -f "${_ssh_public_key}" ]] && LOG FATAL "reading ssh public key path not successful: ${_ssh_public_key}"
    else
        _ssh_public_key="${DEFAULT_SSH_PUBLIC_KEY}"
    fi
    LOG INFO "reading base image root password: IMAGE_ROOT_PASSWORD"
    if [[ -z "${IMAGE_ROOT_PASSWORD}" ]]; then
        LOG DEBUG "IMAGE_ROOT_PASSWORD env not set, using default one"
        _root_password="${DEFAULT_IMAGE_ROOT_PASSWORD}"
    else
        LOG DEBUG "setting base image root password to default one"
        _root_password="${IMAGE_ROOT_PASSWORD}"
    fi

    __debug_str="-vx"
    _base_image="${DEFAULT_BASE_IMAGE}"
    LOG DEBUG "using base image: ${DEFAULT_BASE_IMAGE}"
    _base_size="${DEFAULT_BASE_SIZE}"
    LOG DEBUG "setting base image size: ${DEFAULT_BASE_SIZE}"
    _base_path="${DEFAULT_BASE_IMAGE_PATH}"
    if [[ ! -d "$(dirname $_base_path)" ]]; then
        LOG WARN "base image path does not exist, creating: $(dirname $_base_path)"
        sudo mkdir -p $(dirname $_base_path)
        [[ $? != 0 ]] && LOG FATAL "could not create base image output directory: $(dirname $_base_path)"
    else
        LOG DEBUG "setting base image output path: ${DEFAULT_BASE_IMAGE_PATH}"
    fi
    _base_hostname="${DEFAULT_BASE_HOSTNAME}"
    LOG DEBUG "setting base image hostname: ${DEFAULT_BASE_HOSTNAME}"

    if sudo test -f "$_base_path"; then
        LOG WARN "base image already exists, backing up: ${_base_path}"
        sudo cp "$_base_path" "$(dirname $_base_path)/$(basename $_base_path).bak"
        [[ $? != 0 ]] && LOG ERROR "could not backup file: ${_base_path}"
    fi
    _is_writable "$(dirname $_base_path)"

    _build_image \
        "$__debug_str" \
        "$_root_password" \
        "$_ssh_public_key" \
        "$_base_image" \
        "$_base_size" \
        "$_base_path" \
        "$_base_hostname"
}

function main() {
    build_base_image
}

main "$@"
