#!/usr/bin/env bash
# 
# run script to be used by gitlab runner, for which 
# the config.toml run argument should point to this file.

PRIVATE_SSH_KEY="/home/saliei/.ssh/id_rsa"

CURDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd )"
source ${CURDIR}/base.sh

VM_IP=$(_get_vm_ip)

ssh -i "$PRIVATE_SSH_KEY" -o StrictHostKeyChecking=no gitlab-runner@"$VM_IP" /bin/bash < "${1}"
if [ $? -ne 0 ]; then
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
