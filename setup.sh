#!/usr/bin/env bash
#
# Setup vagrant, libvirt, build a base image and install ansible molecule 
# on a local machine. Register a shell, docker, and a custom gitlab runner
# with a libvirt driver.

BLACK="\033[0;30m"
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
PURPLE="\033[0;35m"
CYAN="\033[0;36m"
WHITE="\033[0;37m"
BGREEN="\033[1;32m"
BRED="\033[1;31m"
RESET="\033[0m"

CURRENT_DIR="$(basename $0)"
GITREPO_URL="https://gitlab.com/saeidgroup/DevEnvSetup"
RUNNERS_SCRIPTS_PATH="/opt/gitlab-runners"
DEFAULT_SSH_PUBLIC_KEY="$HOME/.ssh/id_rsa.pub"
DEFAULT_SSH_PRIVATE_KEY="$HOME/.ssh/id_rsa"
DEFAULT_IMAGE_ROOT_PASSWORD="1234"
DEFAULT_BASE_IMAGE="debian-12"
DEFAULT_BASE_IMAGE_PATH="/var/lib/libvirt/images/gitlab-runner-base-${DEFAULT_BASE_IMAGE}.qcow2"
DEFAULT_BASE_SIZE="12G"
DEFAULT_BASE_HOSTNAME="gitlab-runner-${DEFAULT_BASE_IMAGE}"

trap "DIE" SIGINT
trap "DIE" SIGQUIT

function DIE() {
    echo -e "\nsetup.sh: exiting on interrupt..."
    echo "removing any temporary files"
    for d in "${RUNNERS_SCRIPTS_PATH}/*"; do
        sudo rm -f "$d/tmp-*.tmp"
    done
    sudo rm -f "$(dirname $DEFAULT_BASE_IMAGE_PATH)/tmp-*.tmp"
    exit 127
}

function LOG() {
    CURDATE="${BLUE}$(date +'%Y-%m-%d %T')${RESET}"
    LOGGER="setup"
    ORIG_LOGLEVEL="$1"

    case $1 in
        "DEBUG")
            shift
            LOGLEVEL="${GREEN}DEBUG${RESET}"
            ;;
        "INFO")
            shift
            LOGLEVEL="${CYAN} INFO${RESET}"
            ;;
        "WARN")
            shift
            LOGLEVEL="${YELLOW} WARN${RESET}"
            ;;
        "ERROR")
            shift
            LOGLEVEL="${RED}ERROR${RESET}"
            ;;
        "FATAL")
            shift
            LOGLEVEL="${BRED}FATAL${RESET}"
            ;;
        *)
            LOGLEVEL="${WHITE}NOLEVEL${RESET}"
            ;;
    esac

    LOGMSG="$1"
    echo -e "$CURDATE $LOGGER $LOGLEVEL: $LOGMSG"
    #[[ "$ORIG_LOGLEVEL" == "FATAL" ]] && DIE
}

function _can_run_sudo() {
    if [[ "$EUID" != 0 ]]; then
        LOG WARN "script didn't run with sudo"
        if sudo -n true &>/dev/null; then
            LOG INFO "user can run non-interactive sudo commands"
        else
            LOG ERROR "user can not run non-interactive sudo commands"
        fi
    else
        LOG WARN "script running with sudo" 
    fi
}

function _is_dnf() {
    if ! command -v dnf &> /dev/null; then
        LOG FATAL "no dnf package manager is detected"
    else
        LOG DEBUG "detected dnf package manager"
        _dnf_conf_file="/etc/dnf/dnf.conf"
        if grep -wq "fastestmirror=1" "${_dnf_conf_file}"; then
            LOG INFO "fastestmirror is enabled for dnf"
        else
            LOG WARN "enabling fastest mirror for dnf"
            sudo bash -c "echo 'fastestmirror=1' >> ${_dnf_conf_file}"
        fi
    fi
}

function _create_default_ssh_key() {
    if [[ ! -f "${DEFAULT_SSH_PUBLIC_KEY}" ]] || [[ ! -f "${DEFAULT_SSH_PRIVATE_KEY}" ]]; then
        LOG INFO "default ssh key not present, creating: $HOME/.ssh/id_rsa"
        ssh-keygen -t rsa -b 2048 -f "${DEFAULT_SSH_PRIVATE_KEY}" -N ""
    fi
}

function _is_in_groups_or_add() {
    _username=$1
    if ! id -u $_username &>/dev/null; then
        LOG ERROR "username: $_username does not exists"
    fi

    shift
    for _groupname in "$@"; do
        if ! getent group $_groupname &> /dev/null; then
            LOG WARN "creating group: $_groupname"
            sudo groupadd $_groupname
        fi
        if id -nG "$_username" | grep -qw "$_groupname"; then
            LOG DEBUG "user: $_username already in: $_groupname"
        else
            LOG WARN "adding user: $_username to group: $_groupname"
            sudo usermod -aG $_groupname $_username
            if [[ $? == 0 ]]; then
                LOG WARN "user added to a group, will need relogin" 
            else
                LOG ERROR "failed adding user: $_username to group: $_groupname"
            fi
        fi
    done
}

function _is_writable() {
    _path=$1
    if sudo test -d "${_path}"; then
        _tmp_file="${_path}/tmp-xxxxx.tmp"
        sudo touch "$_tmp_file" &> /dev/null
        if [[ $? == 0 ]]; then
            LOG DEBUG "directory is writable: $_path"
            sudo rm -f "$_tmp_file"
        else
            LOG FATAL "directory is not writable: $_path"
        fi
    else
        LOG WARN "creating directory: ${_path}"
        sudo mkdir -p "${_path}"
        [[ $? != 0 ]] && LOG FATAL "could not create: ${_path}"
    fi
}

function _is_virt_enabled() {
    _hardware_support=$(lscpu | grep 'Virtualization')
    if [[ $_hardware_support ]]; then
        LOG DEBUG "CPU has virtulization support: yes"
        _virtualization=$(lscpu | grep -w 'Virtualization:' | awk '{print $2}')
        if [[ $_virtualization ]]; then
            LOG INFO "detected virtualization: $_virtualization"
        fi
        _virtualization_type=$(lscpu | grep -w 'Virtualization type:' | awk '{print $3}')
        if [[ $_virtualization_type ]]; then
            LOG INFO "detected virtualization type: $_virtualization_type"
        fi
    else
        LOG FATAL "CPU has virtualization support: no"
        DIE
    fi

    _kernel_release=$(uname -r)
    LOG DEBUG "kernel release is: $_kernel_release"
    if [[ -f "/boot/config-$_kernel_release" ]]; then
        _kernel_config_file="/boot/config-$_kernel_release"
        LOG INFO "found kernel compressed config file: $_kernel_config_file"
    elif [[ -f "/proc/config.gz" ]]; then
        _kernel_config_file="/proc/config.gz"
        LOG INFO "found kernel compressed config file: $_kernel_config_file"
    else
        LOG ERROR "could not find kernel compressed config file"
    fi

    _kernel_module=$(zgrep -w "CONFIG_KVM" "$_kernel_config_file")
    if [[ $_kernel_module ]]; then
        _has_kernel_module_support=$(echo "$_kernel_module" | cut -d '=' -f 2)
        if [[ $_has_kernel_module_support == "m"  ]] || [[ $_has_kernel_module_support == "y" ]]; then
            LOG INFO "KVM kernel module is supported: yes"
            _kernel_module_support_type_amd=$(zgrep -w "CONFIG_KVM_AMD" "$_kernel_config_file" | cut -d '=' -f 2)
            if [[ $_kernel_module_support_type_amd == "m"  ]] || [[ $_kernel_module_support_type_amd == "y" ]]; then
                LOG INFO "detected KVM kernel module support for: AMD"
                _has_kernel_module_support_type_amd=1
            else
                _has_kernel_module_support_type_amd=0
            fi
            _kernel_module_support_type_intel=$(zgrep -w "CONFIG_KVM_INTEL" "$_kernel_config_file" | cut -d '=' -f 2)
            if [[ $_kernel_module_support_type_intel == "m"  ]] || [[ $_kernel_module_support_type_intel == "y" ]]; then
                LOG INFO "detected KVM kernel module support for: INTEL"
                _has_kernel_module_support_type_intel=1
            else
                _has_kernel_module_support_type_intel=0
            fi
        else
            LOG ERROR "KVM kernel module is supported: no"
        fi
    else
        LOG ERROR "kernel has no KVM configuration present"
    fi
        
    _is_kvm_autoloaded=$(cat /proc/modules | grep -w kvm)
    if [[ $_is_kvm_autoloaded ]]; then
        LOG INFO "KVM kernel modules are autoloaded: yes"
    else
        LOG WARN "KVM kernel modules are autoloaded: no"
        LOG WARN "attempting to temporarily load kvm kernel module"
        sudo modprobe kvm &> /dev/null
        [[ $? != 0 ]] && LOG ERROR "could not modprobe kvm module"
    fi
    _is_kvm_intel_autoloaded=$(cat /proc/modules | grep -w kvm_intel)
    _is_kvm_amd_autoloaded=$(cat /proc/modules | grep -w kvm_amd)
    if [[ ! $_is_kvm_intel_autoloaded ]] && [[ ! $_is_kvm_amd_autoloaded ]]; then
        _has_kvm_intel_autoloaded=0
        _has_kvm_amd_autoloaded=0
        if [[ $_kernel_module_support_type_intel ]]; then
            LOG WARN "attempting to temporarily load kvm_intel module"
            sudo modprobe kvm_intel &> /dev/null
            _kvm_intel_modprobe_result=$?
            [[ $_kvm_intel_modprobe_result != 0 ]] && LOG ERROR "could not modprobe kvm_intel module"
            [[ $_kvm_intel_modprobe_result == 0 ]] && LOG WARN "modprobed kvm_intel module" && _has_kvm_intel_autoloaded=1
        fi
        if [[ $_kernel_module_support_type_amd ]] && [[ "$_has_kvm_intel_autoloaded" == 0 ]]; then
            LOG WARN "attempting to temporarily load kvm_amd module"
            sudo modprobe kvm_amd &> /dev/null
            _kvm_amd_modprobe_result=$?
            [[ $_kvm_amd_modprobe_result != 0 ]] && LOG ERROR "could not modprobe kvm_amd module"
            [[ $_kvm_amd_modprobe_result == 0 ]] && LOG WARN "modprobed kvm_amd module" && _has_kvm_amd_autoloaded=1
        fi
        if [[ "$_has_kvm_intel_autoloaded" == 0 ]] && [[ "$_has_kvm_amd_autoloaded" == 0 ]]; then
            LOG ERROR "could not modprobe either of kvm_amd nor kvm_intel"
        fi
    fi
}

function _build_image() {
    _debug_str="$1"
    _root_password="$2"
    _ssh_public_key="$3"
    _base_image="$4"
    _base_size="$5"
    _base_path="$6"
    _base_hostname="$7"
    LOG DEBUG "starting virt-builder"
    sudo virt-builder "${_debug_str}" "${_base_image}" \
        --size "${_base_size}" \
        --output "${_base_path}" \
        --hostname "${_base_hostname}" \
        --format qcow2 \
        --network \
        --install curl,sudo,xsltproc,gnupg,software-properties-common,gnupg,xsltproc,genisoimage \
        --install ca-certificates,gcc,iproute2,bzip2,gzip,zip,unzip,tar,xz-utils,openssh-server,libssl-dev \
        --install procps,pkg-config,flake8,yamllint,python3,python3-pip,python3-virtualenv \
        --install qemu-system,libvirt-clients,libvirt-daemon-system,libvirt-dev,ansible,ansible-core \
        --run-command 'curl -fsSL https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg' \
        --run-command 'curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg' \
        --run-command 'gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint' \
        --run-command 'gpg --no-default-keyring --keyring /usr/share/keyrings/docker-archive-keyring.gpg --fingerprint' \
        --run-command 'echo \
                            "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com \
                            "$(. /etc/os-release && echo "$VERSION_CODENAME")" main" | \
                            tee /etc/apt/sources.list.d/hashicorp.list' \
        --run-command 'echo \
                            "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
                            "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
                            tee /etc/apt/sources.list.d/docker.list' \
        --run-command 'curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash' \
        --run-command 'curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh" | bash' \
        --run-command 'useradd -m -p "" gitlab-runner -s /bin/bash' \
        --run-command 'apt update' \
        --install git,git-lfs \
        --install vagrant,terraform \
        --install docker,docker-ce,docker-ce-cli,containerd.io,docker-buildx-plugin,docker-compose-plugin \
        --run-command "git lfs install --skip-repo" \
        --run-command "mkdir -p /opt/tf-state" \
        --run-command "chown -R gitlab-runner:gitlab-runner /opt/tf-state" \
        --run-command "chmod -R 775 /opt/tf-state" \
        --ssh-inject gitlab-runner:file:"${_ssh_public_key}" \
        --run-command "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" \
        --run-command "sed -E 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/' -i /etc/default/grub" \
        --run-command "grub-mkconfig -o /boot/grub/grub.cfg" \
        --run-command "echo 'auto eth0' >> /etc/network/interfaces" \
        --run-command "echo 'allow-hotplug eth0' >> /etc/network/interfaces" \
        --run-command "echo 'iface eth0 inet dhcp' >> /etc/network/interfaces" \
        --run-command "echo 'export PATH=/home/gitlab-runner/.local/bin:/usr/local/bin:/usr/bin:/bin' >> /home/gitlab-runner/.bashrc" \
        --run-command "echo 'export PY_COLORS=true' >> /home/gitlab-runner/.bashrc" \
        --run-command "echo 'export ANSIBLE_FORCE_COLOR=true' >> /home/gitlab-runner/.bashrc" \
        --run-command "adduser gitlab-runner libvirt" \
        --run-command "systemctl enable libvirtd docker" \
        --root-password password:"${_root_password}"

    if [[ $? == 0 ]]; then
        LOG INFO "virt-builder: successful"
    else
        LOG ERROR "virt-builder: not successful"
    fi

}

function _copy_libvirt_config() {
    _username=$1
    _config_path=$2
    _is_writable "${_config_path}"
    sudo chown -R ${_username}:${_username} "${_config_path}"
    __libvirt_config_file="${_config_path}/libvirt.conf"
    if [[ -f "${__libvirt_config_file}" ]]; then
        LOG WARN "file already exists, backing up: ${__libvirt_config_file}"
        cp "${__libvirt_config_file}" "${_config_path}/libvirt.conf.bak"
    fi
    LOG DEBUG "writing libvirt config file: ${__libvirt_config_file}"
    __libvirt_config=''
    __libvirt_config="${__libvirt_config}#uri_aliases = [\n"
    __libvirt_config="${__libvirt_config}#    \"hail=qemu+ssh://root@hail.cloud.example.com/system\",\n"
    __libvirt_config="${__libvirt_config}#    \"snow=qemu+ssh://root@hail.cloud.example.com/system\",\n"
    __libvirt_config="${__libvirt_config}#]\n\n"
    __libvirt_config="${__libvirt_config}uri_default = \"qemu:///system\"\n"
    __libvirt_config="${__libvirt_config}unix_sock_group = \"libvirt\"\n"
    __libvirt_config="${__libvirt_config}unix_sock_rw_perms = \"0770\"\n"
    sudo bash -c "printf '%b' '${__libvirt_config}' > '${__libvirt_config_file}'"
    sudo chown -R ${_username}:${_username} "${__libvirt_config_file}"
}

function _copy_libvirt_scripts() {
    _libvirt_scripts_path=$1
    _ssh_private_key=$2
    _is_writable "$_libvirt_scripts_path"

    _ssh_private_key_escaped=$(echo $_ssh_private_key | sed 's_/_\\/_g')
    __libvirt_script_url_base="${GITREPO_URL}/raw/main/helper-scripts/base.sh"
    __libvirt_script_url_prepare="${GITREPO_URL}/raw/main/helper-scripts/prepare.sh"
    __libvirt_script_url_run="${GITREPO_URL}/raw/main/helper-scripts/run.sh"
    __libvirt_script_url_cleanup="${GITREPO_URL}/raw/main/helper-scripts/cleanup.sh"

    LOG WARN "setting libvirt helper scripts path to: ${_libvirt_scripts_path}"
    LOG INFO "getting libvirt helper script: base.sh"
    sudo curl -fsSL -O --create-dirs --output-dir "${_libvirt_scripts_path}" "${__libvirt_script_url_base}"
    LOG INFO "getting libvirt helper script: prepare.sh"
    sudo curl -fsSL -O --create-dirs --output-dir "${_libvirt_scripts_path}" "${__libvirt_script_url_prepare}"
    LOG INFO "getting libvirt helper script: run.sh"
    sudo curl -fsSL -O --create-dirs --output-dir "${_libvirt_scripts_path}" "${__libvirt_script_url_run}"
    LOG INFO "getting libvirt helper script: cleanup.sh"
    sudo curl -fsSL -O --create-dirs --output-dir "${_libvirt_scripts_path}" "${__libvirt_script_url_cleanup}"

    LOG DEBUG "setting private ssh key on run and prepare helper scripts to: $_ssh_private_key"
    sudo sed -i "0,/.*PRIVATE_SSH_KEY.*/{s/.*PRIVATE_SSH_KEY.*/PRIVATE_SSH_KEY\=\"$_ssh_private_key_escaped\"/}" \
        "${_libvirt_scripts_path}/$(basename ${__libvirt_script_url_prepare})"
    sudo sed -i "0,/.*PRIVATE_SSH_KEY.*/{s/.*PRIVATE_SSH_KEY.*/PRIVATE_SSH_KEY\=\"$_ssh_private_key_escaped\"/}" \
        "${_libvirt_scripts_path}/$(basename ${__libvirt_script_url_run})"

    LOG INFO "setting libvirt helper scripts to be executable"
    for f in "base.sh" "prepare.sh" "run.sh" "cleanup.sh"; do
        sudo chmod +x "${_libvirt_scripts_path}/$f"
        if [[ $? == 0 ]]; then
            LOG WARN "script is executable: ${_libvirt_scripts_path}/$f"
        else
            LOG ERROR "could not set executable bit for script: ${_libvirt_scripts_path}/$f"
        fi
    done
}

function _install_docker() {
    LOG WARN "adding docker fedora repository"
    sudo dnf install -y dnf-plugins-core
    sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    LOG WARN "installing docker engine"
    sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    [[ $? != 0 ]] && LOG FATAL "could not install docker engine"
    sudo systemctl enable --now docker
    sudo systemctl is-active docker --quiet
    [[ $? != 0 ]] && LOG FATAL "error in docker service initialization"
}

function install_virts() {
    LOG DEBUG "starting system update"
    sudo dnf -y update
    LOG DEBUG "starting virtualization group package installation"
    sudo dnf group install -y --with-optional virtualization
    sudo dnf install -y vagrant
    [[ $? != 0 ]] && LOG FATAL "could not install virtulaization package group"
    sudo systemctl enable --now libvirtd
    [[ $? != 0 ]] && LOG FATAL "service start failed: libvirtd"
    sudo systemctl enable --now virtnetworkd --quiet
    [[ $? != 0 ]] && LOG FATAL "service start failed: networkd"

    __username="$(logname)"
    _is_in_groups_or_add "$__username" wheel kvm qemu libvirt
    _copy_libvirt_config "$__username" "$HOME/.config/libvirt"

    __polkit_rules_path="/etc/polkit-1/rules.d"
    __libvirt_polkit_rule_file="${__polkit_rules_path}/50-libvirt.rules"
    __libvirt_polkit_rule=''
    __libvirt_polkit_rule="${__libvirt_polkit_rule}polkit.addRule(function(action, subject) {\n"
    __libvirt_polkit_rule="${__libvirt_polkit_rule}    if (action.id == \"org.libvirt.unix.manage\" &&\n"
    __libvirt_polkit_rule="${__libvirt_polkit_rule}        subject.isInGroup(\"wheel\")) {\n"
    __libvirt_polkit_rule="${__libvirt_polkit_rule}            return polkit.Result.YES;\n"
    __libvirt_polkit_rule="${__libvirt_polkit_rule}    }\n"
    __libvirt_polkit_rule="${__libvirt_polkit_rule}});\n"
    if [[ -d "${__polkit_rules_path}" ]]; then
        LOG WARN "writing libvirt polkit rule: ${__libvirt_polkit_rule_file}"
        sudo bash -c "printf '%b' '${__libvirt_polkit_rule}' > '${__libvirt_polkit_rule_file}'"
        LOG WARN "added a new polkit rule, will need relogin"
    else
        LOG WARN "directory does not exist: ${__polkit_rules_path}"
        LOG ERROR "could not write libvirt polkit rule: ${__libvirt_polkit_rule_file}"
    fi

    LOG WARN "restarting libvirtd service"
    sudo systemctl restart libvirtd --quiet
    sleep 2
    if systemctl is-active libvirtd --quiet; then 
        LOG INFO "restarting libvirtd service: successful"
    else
        LOG ERROR "restarting libvirtd service: not successful"
    fi

    if vagrant plugin list | grep -w vagrant-libvirt &>/dev/null; then
        LOG INFO "vagrant libvirt plugin already installed"
    else
        LOG INFO "installing vagrant libvirt plugin"
        vagrant plugin install vagrant-libvirt
        [[ $? != 0 ]] && LOG ERROR "vagrant-libvirt plugin install: not successful"
    fi
}

function install_ansible() {
    LOG INFO "installing python and pip"
    sudo dnf install -y python3 python3-pip 
    [[ $? != 0 ]] && LOG ERROR "installing python and pip: not successful"
    LOG INFO "pip installing ansible suit"
    pip install --no-input ansible ansible-core ansible-lint
    [[ $? != 0 ]] && LOG ERROR "pip installing ansible suit: not successful"
    LOG INFO "pip installing molecule and libvirt provider interfaces"
    pip install --no-input molecule molecule-vagrant libvirt-python python-vagrant docker pytest-testinfra
    [[ $? != 0 ]] && LOG ERROR "pip installing molecule and libvirt provider interfaces: not successful"

    _bashrc_file="$HOME/.bashrc"
    if ! grep -wq "export PY_COLORS=true" $_bashrc_file; then
        LOG WARN "appending py colors env to bashrc"
        echo "export PY_COLORS=true" >> $_bashrc_file
    fi
    if ! grep -wq "ANSIBLE_FORCE_COLOR=true" $_bashrc_file; then
        LOG WARN "appending ansible color envs to bashrc"
        echo "export ANSIBLE_FORCE_COLOR=true" >> $_bashrc_file
    fi
}

function register_gitlab_runners() {
    _install_docker

    LOG DEBUG "starting gitlab registration"
    LOG INFO "reading ssh private key path: SSH_PRIVATE_KEY"
    if [[ -z "${SSH_PRIVATE_KEY}" ]]; then
        LOG WARN "SSH_PRIVATE_KEY env not set, using default one: ${DEFAULT_SSH_PRIVATE_KEY}"
        _create_default_ssh_key
        _ssh_private_key=${DEFAULT_SSH_PRIVATE_KEY}
        [[ ! -f "${_ssh_private_key}" ]] && LOG FATAL "reading ssh private key path not successful: ${_ssh_private_key}"
    else
        _ssh_private_key="${DEFAULT_SSH_PRIVATE_KEY}"
    fi

    LOG INFO "adding gitlab runner repository"
    curl -fsSL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
    [[ $? != 0 ]] && LOG FATAL "could not add gitlab runner repository"
    LOG INFO "updating and installing gitlab-runner"
    sudo dnf update -y
    sudo dnf install -y --nogpgcheck gitlab-runner
    [[ $? != 0 ]] && LOG FATAL "could not install gitlab-runner package"
    sudo systemctl enable --now gitlab-runner 
    sudo systemctl is-active gitlab-runner --quiet
    [[ $? != 0 ]] && LOG FATAL "error in gitlab-runner service initialization"

    if id -u gitlab-runner &>/dev/null; then
        LOG WARN "user gitlab-user exists: updating"
        sudo usermod -c "Gitlab Runner" gitlab-runner &>/dev/null
        sudo usermod -s /bin/bash gitlab-runner &>/dev/null
    else
        LOG WARN "user gitlab-runner doesn't exist: creating"
        sudo useradd --comment "Gitlab Runner" --shell /bin/bash --create-home /home/gitlab-runner
    fi

    LOG INFO "reading shell runner register token from env: SHELL_RUNNER_TOKEN"
    if [[ -z "${SHELL_RUNNER_TOKEN}" ]]; then
        LOG DEBUG "SHELL_RUNNER_TOKEN env not set, prompting user to enter"
        read -p "please enter shell runner token obtained from gitlab:" SHELL_RUNNER_TOKEN
        [[ -z "${SHELL_RUNNER_TOKEN}" ]] && LOG FATAL "shell runner token reading not successful"
    fi
    LOG INFO "reading docker runner token from env: DOCKER_RUNNER_TOKEN"
    if [[ -z "${DOCKER_RUNNER_TOKEN}" ]]; then
        LOG DEBUG "DOCKER_RUNNER_TOKEN env not set, prompting user to enter"
        read -p "please enter docker runner token obtained from gitlab:" DOCKER_RUNNER_TOKEN
        [[ -z "${DOCKER_RUNNER_TOKEN}" ]] && LOG FATAL "libvirt runner token reading not successful"
    fi
    LOG INFO "reading custom runner token from env: LIBVIRT_RUNNER_TOKEN"
    if [[ -z "${LIBVIRT_RUNNER_TOKEN}" ]]; then
        LOG DEBUG "LIBVIRT_RUNNER_TOKEN env not set, prompting user to enter"
        read -p "please enter custom runner token obtained from gitlab:" LIBVIRT_RUNNER_TOKEN
        [[ -z "${LIBVIRT_RUNNER_TOKEN}" ]] && LOG FATAL "libvirt runner token reading not successful"
    fi

    __gitlab_runner_config_file="/etc/gitlab-runner/config.toml"
    if sudo test -f "${__gitlab_runner_config_file}"; then
        LOG WARN "file already exists, backing up: ${__gitlab_runner_config_file}"
        sudo cp "$__gitlab_runner_config_file" "$(dirname $__gitlab_runner_config_file)/config.toml.bak"
        [[ $? != 0 ]] && LOG ERROR "could not backup file: $__gitlab_runner_config_file"
        LOG WARN "removing file: ${__gitlab_runner_config_file}"
        sudo rm -f "${__gitlab_runner_config_file}"
    fi
    
    __gitlab_builds_dir="/home/gitlab-runner/builds"
    __gitlab_cache_dir="/home/gitlab-runner/cache"
    _is_writable "${__gitlab_builds_dir}"
    _is_writable "${__gitlab_cache_dir}"
    sudo chown -R gitlab-runner:gitlab-runner "${__gitlab_builds_dir}"
    sudo chown -R gitlab-runner:gitlab-runner "${__gitlab_cache_dir}"

    LOG INFO "registering a shell gitlab runner"
    __shell_runner_scripts_path="${RUNNERS_SCRIPTS_PATH}/shell-driver"
    _is_writable $__shell_runner_scripts_path
    sudo gitlab-runner register --non-interactive \
        --url "https://gitlab.com" \
        --token "${SHELL_RUNNER_TOKEN}" \
        --executor "shell" \
        --builds-dir "${__gitlab_builds_dir}" \
        --cache-dir "${__gitlab_cache_dir}" \
        --description "Shell Runner" \

    LOG INFO "registering a docker gitlab runner"
    __docker_runner_scripts_path="${RUNNERS_SCRIPTS_PATH}/docker-driver"
    _is_writable $__docker_runner_scripts_path
    sudo gitlab-runner register --non-interactive \
        --url "https://gitlab.com" \
        --token "${DOCKER_RUNNER_TOKEN}" \
        --executor "docker" \
        --docker-image "alpine:latest" \
        --builds-dir "${__gitlab_builds_dir}" \
        --cache-dir "${__gitlab_cache_dir}" \
        --description "Docker Runner" \

    __libvirt_runner_scripts_path="${RUNNERS_SCRIPTS_PATH}/libvirt-driver"
    _is_writable $__libvirt_runner_scripts_path
    _copy_libvirt_scripts $__libvirt_runner_scripts_path $_ssh_private_key

    LOG INFO "registering a custom (libvirt) gitlab runner"
    sudo gitlab-runner register --non-interactive \
        --url "https://gitlab.com" \
        --token "${LIBVIRT_RUNNER_TOKEN}" \
        --executor "custom" \
        --builds-dir "${__gitlab_builds_dir}" \
        --cache-dir "${__gitlab_cache_dir}" \
        --description "libvirt-driver" \
        --custom-prepare-exec "${__libvirt_runner_scripts_path}/prepare.sh" \
        --custom-run-exec "${__libvirt_runner_scripts_path}/run.sh" \
        --custom-cleanup-exec "${__libvirt_runner_scripts_path}/cleanup.sh"

    sudo systemctl restart gitlab-runner --quiet
    sudo systemctl is-active gitlab-runner --quiet
    if [[ $? == 0 ]];then
        LOG INFO "gitlab-runner service start: successful"
    else
        LOG FATAL "gitlab-runner service start: not successful"
    fi

    _is_in_groups_or_add gitlab-runner wheel kvm qemu libvirt docker
    _copy_libvirt_config gitlab-runner "/home/gitlab-runner/.config/libvirt"

    __tf_state_dir="/opt/tf-state"
    _is_writable "${__tf_state_dir}"
    sudo chown -R gitlab-runner:gitlab-runner "${__tf_state_dir}"
    sudo chmod -R 775 "${__tf_state_dir}"

    sudo systemctl restart docker --quiet
    sudo systemctl is-active docker --quiet
    if [[ $? == 0 ]];then
        LOG INFO "docker service start: successful"
    else
        LOG FATAL "docker service start: not successful"
    fi
}

function build_base_image() {
    LOG DEBUG "starting base image builder"
    LOG INFO "reading ssh public key path: SSH_PUBLIC_KEY"
    if [[ -z "${SSH_PUBLIC_KEY}" ]]; then
        LOG WARN "SSH_PUBLIC_KEY env not set, using default one: ${DEFAULT_SSH_PUBLIC_KEY}"
        _create_default_ssh_key
        _ssh_public_key=${DEFAULT_SSH_PUBLIC_KEY}
        [[ ! -f "${_ssh_public_key}" ]] && LOG FATAL "reading ssh public key path not successful: ${_ssh_public_key}"
    else
        _ssh_public_key="${DEFAULT_SSH_PUBLIC_KEY}"
    fi
    LOG INFO "reading base image root password: IMAGE_ROOT_PASSWORD"
    if [[ -z "${IMAGE_ROOT_PASSWORD}" ]]; then
        LOG DEBUG "IMAGE_ROOT_PASSWORD env not set, using default one"
        _root_password="${DEFAULT_IMAGE_ROOT_PASSWORD}"
    else
        LOG DEBUG "setting base image root password to default one"
        _root_password="${IMAGE_ROOT_PASSWORD}"
    fi

    __debug_str="-vx"
    _base_image="${DEFAULT_BASE_IMAGE}"
    LOG DEBUG "using base image: ${DEFAULT_BASE_IMAGE}"
    _base_size="${DEFAULT_BASE_SIZE}"
    LOG DEBUG "setting base image size: ${DEFAULT_BASE_SIZE}"
    _base_path="${DEFAULT_BASE_IMAGE_PATH}"
    if [[ ! -d "$(dirname $_base_path)" ]]; then
        LOG WARN "base image path does not exist, creating: $(dirname $_base_path)"
        sudo mkdir -p $(dirname $_base_path)
        [[ $? != 0 ]] && LOG FATAL "could not create base image output directory: $(dirname $_base_path)"
    else
        LOG DEBUG "setting base image output path: ${DEFAULT_BASE_IMAGE_PATH}"
    fi
    _base_hostname="${DEFAULT_BASE_HOSTNAME}"
    LOG DEBUG "setting base image hostname: ${DEFAULT_BASE_HOSTNAME}"

    if sudo test -f "$_base_path"; then
        LOG WARN "base image already exists, backing up: ${_base_path}"
        sudo cp "$_base_path" "$(dirname $_base_path)/$(basename $_base_path).bak"
        [[ $? != 0 ]] && LOG ERROR "could not backup file: ${_base_path}"
    fi
    _is_writable "$(dirname $_base_path)"

    _build_image \
        "$__debug_str" \
        "$_root_password" \
        "$_ssh_public_key" \
        "$_base_image" \
        "$_base_size" \
        "$_base_path" \
        "$_base_hostname"
}

function print_usage() {
    echo "Usage: $0 [options]"
    echo "Install libvirt/qemu/KVM, ansible/molecule stack, build a base image, and"
    echo "register a shell, docker, and a custom gitlab CI/CD runner with libvirt driver."
    echo ""
    echo "options:"
    echo "  --virts     install libvirt/qemu/KVM stack."
    echo "  --ansible   install ansible and molecule stack."
    echo "  --build     build a base qemu image for gitlab's CI/CD pipeline."
    echo "  --register  register shell, docker, and libvirt gitlab runners."
    echo "  --help|-h   print this help message."
    echo ""
    echo "defaults to all of above in order."
}

function main() {
    if [[ $# == 0 ]]; then
        _can_run_sudo
        _is_dnf
        _is_virt_enabled
        install_virts
        install_ansible
        build_base_image
        register_gitlab_runners
    else
        while [[ $# > 0 ]]; do
            case "$1" in
                "--virts")
                    _can_run_sudo
                    _is_dnf
                    _is_virt_enabled
                    install_virts
                    ;;
                "--ansible")
                    _can_run_sudo
                    _is_dnf
                    install_ansible
                    ;;
                "--build")
                    _can_run_sudo
                    _is_dnf
                    _is_virt_enabled
                    install_virts
                    build_base_image
                    ;;
                "--register")
                    _can_run_sudo
                    _is_dnf
                    _is_virt_enabled
                    install_virts
                    register_gitlab_runners
                    ;;
                "--help" | "-h")
                    print_usage
                    exit 0
                    ;;
                *)
                    print_usage
                    exit 1
                    ;;
            esac
            shift
        done
    fi
}

main "$@"
